(function() {
    'use strict';
    
    angular
        .module('gemStore')
        .directive('gallery', gallery);
        
    function gallery() {
        
        var directive = {
            restrict: 'E',
            templateUrl: 'app/gem-store/gallery/gallery.html',
            controller: GalleryController,
            controllerAs: 'gallery',
            scope: {
                images: '='
            },
            bindToController: true
        }
        
        return directive;
        
    }
    
    function GalleryController() {
        var vm = this;
        vm.hasImages = hasImages();
        vm.current = getFirst();
        vm.setCurrent = setCurrent;
        
        function hasImages() {
            return vm.images.length;
        }
        
        function getFirst() {
            if (hasImages())
                return vm.images[0];
        }
        
        function setCurrent(current) {
            vm.current = current || getFirst();
        }
    }
    
})();