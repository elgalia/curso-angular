(function() {
    'use strict';
    
    angular
        .module('gemStore')
        .directive('description', description);
        
    function description() {
        
        var directive = {
            restrict: 'E',
            templateUrl: 'app/gem-store/description/description.html',
            transclude: true
        }
        
        return directive;
        
    }
    
})();