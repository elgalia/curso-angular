(function() {
    'use strict';
    
    angular
        .module('gemStore')
        .directive('reviews', reviews);
        
    function reviews() {
        
        var directive = {
            restrict: 'E',
            templateUrl: 'app/gem-store/reviews/reviews.html',
            controller: ReviewController,
            controllerAs: 'reviewCtrl',
            scope: {
                reviews: '=collection'
            },
            bindToController: true
        }
        
        return directive;
        
    }
    
    function ReviewController() {
        var vm = this;
        vm.addReview = addReview;
        reset();
        
        function reset() {
            vm.review = {};
        };
        
        function addReview(form) {
            if (form.$invalid) return;
            
            vm.review.createdOn = Date.now();
            vm.reviews.push(vm.review);
            reset();
            
            form.$setPristine();
        }
    }
    
})();